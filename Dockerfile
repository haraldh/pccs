# Import the Intel signing key
FROM ubuntu:jammy
RUN mkdir -p /etc/apt/trusted.gpg.d
COPY intel-sgx.gpg.asc nodesource-repo.gpg.asc /etc/apt/keyrings/
COPY nodesource.list intel-sgx.list /etc/apt/sources.list.d/

RUN apt update \
 && apt install --no-install-recommends -y ca-certificates \
 && apt install -y curl gnupg

WORKDIR /opt/intel/sgx-dcap-pccs
RUN mkdir /etc/init

RUN apt update \
    && DEBIAN_FRONTEND=noninteractive apt -q --no-install-recommends  -y install \
    nodejs sgx-dcap-pccs jq python3

# cleanup
RUN rm -rf /var/lib/apt/lists/* && rm -rf /tmp/*

# configure nodejs
WORKDIR /opt/intel/sgx-dcap-pccs
USER pccs
RUN npm i sqlite3 -D && rm -rf node_modules && npm i && npm rebuild

USER root
# Generate a self-signed cert
COPY cert_ext.cnf /opt/intel/sgx-dcap-pccs/ssl_key/cert_ext.cnf
RUN openssl genrsa -out ssl_key/private.pem 2048 \
	&& openssl req -new -key ssl_key/private.pem -out ssl_key/csr.pem -config ssl_key/cert_ext.cnf \
	&& openssl x509 -req -days 3650 -in ssl_key/csr.pem -signkey ssl_key/private.pem -out ssl_key/file.crt

COPY config.json /opt/intel/sgx-dcap-pccs/config/default.json
COPY start.sh /opt/intel/sgx-dcap-pccs/start.sh

EXPOSE 8081

CMD /opt/intel/sgx-dcap-pccs/start.sh
