# How to use this container

Get an Intel APIKEY:

- go to https://api.portal.trustedservices.intel.com/
- sign up or sign in
- then go to https://api.portal.trustedservices.intel.com/Products/liv-intel-software-guard-extensions-provisioning-certification-service
and subscribe to the service

Set your Intel APIKEY (primary or secondary) as a podman secret once:

```
$ PCCS_CONFIG='<YOUR JSON CONFIG CHANGES>' podman secret create --env PCCS_CONFIG PCCS_CONFIG
```
e.g.:
```
$ PCCS_CONFIG='{"ApiKey": "xxxx", "UserTokenHash" : "xxxx", "AdminTokenHash" : "xxxx"}' podman secret create --env PCCS_CONFIG PCCS_CONFIG
```

Then start the container with: 
```
$ podman run \
    -it --rm -v /dev/log:/dev/log \
    --secret PCCS_CONFIG,type=mount \
    --net host \
    registry.gitlab.com/haraldh/pccs
```
The above command will run the container interactively (`-it`) on your system.
It will also remove the container image when execution is complete (`--rm`).

You can tweak these arguments as necessary.
