#!/bin/bash
set -e
. /etc/environment
cd /opt/intel/sgx-dcap-pccs

jq '. * input' config/default.json /run/secrets/PCCS_CONFIG > config/default.json.new
mv config/default.json.new config/default.json

exec /usr/bin/node /opt/intel/sgx-dcap-pccs/pccs_server.js
